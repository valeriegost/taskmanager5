package ru.volnenko.tm.controller;

import ru.volnenko.tm.service.DomainService;

public class SystemController {

    private DomainService domainService;

    public SystemController(DomainService domainService) {
        this.domainService = domainService;
    }

    public int dataLoadJSON() throws Exception{
        System.out.println("[DATA LOAD JSON]");
        domainService.dataLoadJSON();
        System.out.println("[OK]");
        return 0;
    }

    public int dataSaveJSON() throws Exception{
        System.out.println("[DATA SAVE JSON]");
        domainService.dataSaveJSON();
        System.out.println("[OK]");
        return 0;
    }

    public int dataLoadXML() throws Exception{
        System.out.println("[DATA LOAD XML]");
        domainService.dataLoadXML();
        System.out.println("[OK]");
        return 0;
    }

    public int dataSaveXML() throws Exception{
        System.out.println("[DATA SAVE XML]");
        domainService.dataSaveXML();
        System.out.println("[OK]");
        return 0;
    }

    public int dataLoadBin() throws Exception{
        System.out.println("[DATA LOAD BIN]");
        domainService.dataLoadBin();
        System.out.println("[OK]");
        return 0;
    }

    public int dataSaveBin() throws Exception{
        System.out.println("[DATA SAVE BIN]");
        domainService.dataSaveBin();
        System.out.println("[OK]");
        return 0;
    }

    public int displayExit() {
        System.out.println("Terminate program...");
        return 0;
    }

    public int displayError() {
        System.out.println("Error! Unknown program argument...");
        return -1;
    }

    public void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    public int displayHelp() {
        System.out.println("version - Display application version.");
        System.out.println("about - Display developer info.");
        System.out.println("help - Display list of commands.");
        System.out.println("exit - Terminate console application.");
        System.out.println();
        System.out.println("project-list - Display list of projects.");
        System.out.println("project-create - Create new project by name.");
        System.out.println("project-clear - Remove all projects.");
        System.out.println();
        System.out.println("task-list - Display list of tasks.");
        System.out.println("task-create - Create new task by name.");
        System.out.println("task-clear - Remove all tasks.");
        return 0;
    }

    public int displayVersion() {
        System.out.println("1.0.0");
        return 0;
    }

    public int displayAbout() {
        System.out.println("Denis Volnenko");
        System.out.println("denis@volnenko.ru");
        return 0;
    }

}
