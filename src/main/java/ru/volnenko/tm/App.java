package ru.volnenko.tm;

import ru.volnenko.tm.controller.ProjectController;
import ru.volnenko.tm.controller.SystemController;
import ru.volnenko.tm.controller.TaskController;
import ru.volnenko.tm.endpoint.ProjectEndpoint;
import ru.volnenko.tm.endpoint.TaskEndpoint;
import ru.volnenko.tm.repository.ProjectRepository;
import ru.volnenko.tm.repository.TaskRepository;
import ru.volnenko.tm.service.DomainService;
import ru.volnenko.tm.service.ProjectService;
import ru.volnenko.tm.service.TaskService;

import javax.xml.ws.Endpoint;
import java.util.Scanner;

import static ru.volnenko.tm.constant.TerminalConst.*;

/**
 * Тестовое приложение
 */
public class App {

    private final ProjectRepository projectRepository = new ProjectRepository();

    private final TaskRepository taskRepository = new TaskRepository();

    private final ProjectService projectService = new ProjectService(projectRepository);

    private final ProjectController projectController = new ProjectController(projectService);

    private final TaskService taskService = new TaskService(taskRepository);

    private final TaskController taskController = new TaskController(taskService);

    private final DomainService domainService= new DomainService(projectService, taskService);

    private final SystemController systemController = new SystemController(domainService);

    private final ProjectEndpoint projectEndPoint = new ProjectEndpoint(projectService);

    private final TaskEndpoint taskEndpoint = new TaskEndpoint(taskService);

    {
        projectRepository.create("DEMO PROJECT 1");
        projectRepository.create("DEMO PROJECT 2");
        taskRepository.create("TEST TASK 1");
        taskRepository.create("TEST TASK 2");
    }

    public static void main(final String[] args) throws Exception{
        final Scanner scanner = new Scanner(System.in);
        final App app = new App();
        app.run(args);
        Endpoint.publish("http://0.0.0.0:8080/ProjectEndpoint?wsdl", app.projectEndPoint);
        Endpoint.publish("http://0.0.0.0:8080/TaskEndpoint?wsdl", app.taskEndpoint);
        app.systemController.displayWelcome();
        String command = "";
        while (!EXIT.equals(command)) {
            command = scanner.nextLine();
            app.run(command);
        }
    }

    public void run(final String[] args) throws Exception{
        if (args == null) return;
        if (args.length < 1) return;
        final String param = args[0];
        final int result = run(param);
        System.exit(result);
    }

    public int run(final String param) throws Exception{
        if (param == null || param.isEmpty()) return -1;
        switch (param) {
            case VERSION: return systemController.displayVersion();
            case ABOUT: return  systemController.displayAbout();
            case HELP: return  systemController.displayHelp();
            case EXIT: return  systemController.displayExit();

            case DATA_LOAD_JSON: return systemController.dataLoadJSON();
            case DATA_SAVE_JSON: return systemController.dataSaveJSON();
            case DATA_LOAD_XML: return systemController.dataLoadXML();
            case DATA_SAVE_XML: return systemController.dataSaveXML();
            case DATA_LOAD_BIN: return systemController.dataLoadBin();
            case DATA_SAVE_BIN: return systemController.dataSaveBin();

            case PROJECT_LIST: return projectController.listProject();
            case PROJECT_CLEAR: return projectController.clearProject();
            case PROJECT_CREATE: return projectController.createProject();
            case PROJECT_VIEW: return projectController.viewProjectByIndex();
            case PROJECT_REMOVE_BY_NAME: return projectController.removeProjectByName();
            case PROJECT_REMOVE_BY_ID: return projectController.removeProjectById();
            case PROJECT_REMOVE_BY_INDEX: return projectController.removeProjectByIndex();
            case PROJECT_UPDATE_BY_INDEX: return projectController.updateProjectByIndex();

            case TASK_LIST: return taskController.listTask();
            case TASK_CLEAR: return taskController.clearTask();
            case TASK_CREATE: return taskController.createTask();
            case TASK_VIEW: return taskController.viewTaskByIndex();

            default: return systemController.displayError();
        }
    }

}
