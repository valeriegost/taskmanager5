package ru.volnenko.tm.service;

import org.junit.Assert;
import org.junit.Test;
import ru.volnenko.tm.entity.Task;
import ru.volnenko.tm.repository.TaskRepository;

public class TaskServiceTest {
    private final TaskRepository taskRepository = new TaskRepository();
    private final TaskService taskService = new TaskService(taskRepository);

    @Test
    public void testCRUD() {
        Assert.assertTrue(taskService.findAll().isEmpty());
        taskService.create("Task1");
        Assert.assertFalse(taskService.findAll().isEmpty());
        Assert.assertNotNull(taskService.findByName("Task1"));
        Assert.assertNotNull(taskService.removeByName("Task1"));
        Assert.assertNull(taskService.removeByName("Task1"));
        Task testTask = taskService.create("Task2");
        Assert.assertNotNull(taskService.removeById(testTask.getId()));
        Assert.assertNull(taskService.removeById(testTask.getId()));
    }
}
