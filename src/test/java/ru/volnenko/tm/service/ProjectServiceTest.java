package ru.volnenko.tm.service;

import org.junit.Assert;
import org.junit.Test;
import ru.volnenko.tm.repository.ProjectRepository;

public class ProjectServiceTest {

    private final ProjectRepository projectRepository = new ProjectRepository();
    private final ProjectService projectService = new ProjectService(projectRepository);

    @Test
    public void testCRUD() {
        Assert.assertTrue(projectService.findAll().isEmpty());
        projectService.create("Project1");
        Assert.assertFalse(projectService.findAll().isEmpty());
        Assert.assertNotNull(projectService.findByName("Project1"));
        Assert.assertTrue(projectService.findByName("Project1").getDescription() == null || projectService.findByName("Project1").getDescription() == "");
        projectService.update(projectService.findByName("Project1").getId(), "Project1", "This is project1");
        Assert.assertFalse(projectService.findByName("Project1").getDescription() == null || projectService.findByName("Project1").getDescription() == "");
        Assert.assertNotNull(projectService.removeByName("Project1"));
        Assert.assertNull(projectService.removeByName("Project1"));
    }
}
