package ru.volnenko.tm.repository;

import org.junit.Assert;
import org.junit.Test;
import ru.volnenko.tm.entity.Task;

public class TaskRepositoryTest {
    private final TaskRepository taskRepository = new TaskRepository();

    @Test
    public void testCRUD() {
        Assert.assertTrue(taskRepository.findAll().isEmpty());
        taskRepository.create("Task1");
        Assert.assertFalse(taskRepository.findAll().isEmpty());
        Assert.assertNotNull(taskRepository.findByName("Task1"));
        Assert.assertNotNull(taskRepository.removeByName("Task1"));
        Assert.assertNull(taskRepository.removeByName("Task1"));
        Task testTask = taskRepository.create("Task2");
        Assert.assertNotNull(taskRepository.removeById(testTask.getId()));
        Assert.assertNull(taskRepository.removeById(testTask.getId()));
    }
}
