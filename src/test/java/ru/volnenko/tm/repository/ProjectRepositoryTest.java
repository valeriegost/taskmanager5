package ru.volnenko.tm.repository;

import org.junit.Assert;
import org.junit.Test;

public class ProjectRepositoryTest {

    private final ProjectRepository projectRepository = new ProjectRepository();

    @Test
    public void testCRUD() {
        Assert.assertTrue(projectRepository.findAll().isEmpty());
        projectRepository.create("Project1");
        Assert.assertFalse(projectRepository.findAll().isEmpty());
        Assert.assertNotNull(projectRepository.findByName("Project1"));
        Assert.assertTrue(projectRepository.findByName("Project1").getDescription() == null || projectRepository.findByName("Project1").getDescription() == "");
        projectRepository.update(projectRepository.findByName("Project1").getId(), "Project1", "This is project1");
        Assert.assertFalse(projectRepository.findByName("Project1").getDescription() == null || projectRepository.findByName("Project1").getDescription() == "");
        Assert.assertNotNull(projectRepository.removeByName("Project1"));
        Assert.assertNull(projectRepository.removeByName("Project1"));
    }
}
