package ru.volnenko.tm.endpoint;

import org.junit.Assert;
import org.junit.Test;
import ru.volnenko.tm.entity.Task;
import ru.volnenko.tm.repository.TaskRepository;
import ru.volnenko.tm.service.TaskService;

public class TaskEndpointTest {

    private final TaskRepository taskRepository = new TaskRepository();
    private final TaskService taskService = new TaskService(taskRepository);
    private final TaskEndpoint taskEndpoint = new TaskEndpoint(taskService);

    @Test
    public void testCRUD() {
        Assert.assertTrue(taskEndpoint.findAll().isEmpty());
        taskEndpoint.create("Task1");
        Assert.assertFalse(taskEndpoint.findAll().isEmpty());
        Assert.assertNotNull(taskEndpoint.findByName("Task1"));
        Assert.assertNotNull(taskEndpoint.removeByName("Task1"));
        Assert.assertNull(taskEndpoint.removeByName("Task1"));
        Task testTask = taskEndpoint.create("Task2");
        Assert.assertNotNull(taskEndpoint.removeById(testTask.getId()));
        Assert.assertNull(taskEndpoint.removeById(testTask.getId()));
    }
}
