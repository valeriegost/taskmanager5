package ru.volnenko.tm.endpoint;

import org.junit.Assert;
import org.junit.Test;
import ru.volnenko.tm.repository.ProjectRepository;
import ru.volnenko.tm.service.ProjectService;

public class ProjectEndpointTest {

    private final ProjectRepository projectRepository = new ProjectRepository();
    private final ProjectService projectService = new ProjectService(projectRepository);
    private final ProjectEndpoint projectEndpoint = new ProjectEndpoint(projectService);

    @Test
    public void testCRUD() {
        Assert.assertTrue(projectEndpoint.findAll().isEmpty());
        projectEndpoint.create("Project1", "Project1 description");
        Assert.assertFalse(projectEndpoint.findAll().isEmpty());
        Assert.assertNotNull(projectEndpoint.findByName("Project1"));
        Assert.assertTrue(projectEndpoint.findByName("Project1").getDescription() != null && projectEndpoint.findByName("Project1").getDescription() == "Project1 description");
        projectEndpoint.update(projectEndpoint.findByName("Project1").getId(), "Project1", "This is project1");
        Assert.assertTrue(projectEndpoint.findByName("Project1").getDescription() != null && projectEndpoint.findByName("Project1").getDescription() == "This is project1");
        Assert.assertNotNull(projectEndpoint.removeByName("Project1"));
        Assert.assertNull(projectEndpoint.removeByName("Project1"));
    }
}
